#
# GitLab CI: C++ compilers + meson
#
# https://hub.docker.com/r/nativeci/cpp-meson/
#

FROM ubuntu:22.04
LABEL maintainer="Jens Finkhaeuser <jens@finkhaeuser.de>"

ENV DEBIAN_FRONTEND noninteractive

# Base OS
RUN apt-get -qq update \
 && apt-get install -qqy --no-install-recommends \
      apt-utils \
      ca-certificates \
      gnupg2 \
      wget

RUN apt-get -qq update \
 && apt-get upgrade -qqy --no-install-recommends
RUN apt-get install -qqy --no-install-recommends \
      locales \
      libc-bin \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN locale-gen en_US.UTF-8
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

# Dependencies
RUN wget -qO- https://apt.llvm.org/llvm-snapshot.gpg.key | tee /etc/apt/trusted.gpg.d/llvm-snapshot.gpg.asc

ADD clang.list /etc/apt/sources.list.d

RUN apt-get -qq update

ARG TARGETARCH
RUN if test "${TARGETARCH}" = "amd64" ; then \
      apt-get install -qqy --no-install-recommends \
      gcc-multilib \
      g++-multilib ;\
    fi

RUN apt-get install -qqy --no-install-recommends \
      build-essential \
      libbsd-dev \
      ninja-build \
      python3 \
      python3-pip \
      clang-18 \
      lld-18 \
      lldb-18 \
      libclang-rt-18-dev \
      gcc \
      gcovr \
      lcov \
      git \
      pkg-config \
      cmake \
      nodejs \
      valac \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Provide symlinks for the llvm/clang stuff
RUN cd /usr/bin \
 && for name in *-18 ; do \
      if readlink $name | grep llvm >/dev/null ; then \
        short=$(echo $name | sed 's/-18//g') ;\
        ln -snf $name $short ;\
      fi ;\
    done

RUN pip3 install meson pipenv conan
RUN conan remote add -f conancenter https://center.conan.io
