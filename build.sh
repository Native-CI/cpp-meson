#!/bin/sh

REL="${1}"
if test -z "${REL}" ; then REL=0 ; fi

NET="${2}"
if test -z "${NET}" ; then NET=host ; fi


docker build --network="${NET}" \
    --tag=nativeci/cpp-meson:release-$(date +%Y%m%d).${REL} \
    --tag=nativeci/cpp-meson:latest \
    .
