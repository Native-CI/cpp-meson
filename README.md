# C++ + meson + conan
[![status-badge](https://ci.codeberg.org/api/badges/Native-CI/cpp-meson/status.svg)](https://ci.codeberg.org/Native-CI/cpp-meson)
![arch|amd64](https://img.shields.io/badge/arch-amd64-green)
![arch|arm64](https://img.shields.io/badge/arch-arm64-green)

GitLab CI image for C/C++ projects with [meson](https://mesonbuild.com)

- Based on Ubuntu LTS
- Installs [GCC](https://gcc.gnu.org/) and [Clang](https://clang.llvm.org/) based C/C++ compilers
- Installs `valac`, the [Vala](https://vala.dev/) compiler
- Installs coverage tools
- Installs meson and [ninja](https://ninja-build.org/)
- Installs [conan](https://conan.io/) for native dependency management and/or package builds

## Usage

See package on codeberg: [Native-CI/cpp-meson](https://codeberg.org/Native-CI/-/packages/container/cpp-meson/latest)

```bash
$ docker pull nativeci/cpp-meson
```

## Build Instructions

```bash
$ docker network ls
```

Pick a known good network, say `host`. This is a good choice in most cases.

Increment the `.0` in the command below as necesary. The CI will use the build
number here, so if you're building on the same day as the CI, best pick another
pattern.

```bash
$ docker build --network=host \
    --tag=nativeci/cpp-meson:release-$(date +%Y%m%d).0 \
    --tag=nativeci/cpp-meson:latest \
    .
```

Or see `build.sh` - that simplifies it mildly.

Happy? Push the image with all tags.

```bash
$ docker push -a nativeci/cpp-meson
```

# Note

Older builds are pushed to docker hub as `jfinkhaeuser/gitlab-ci-cpp-meson`.
